$(document).ready(function() {

  if( !Modernizr.objectfit ) {

    $(".box-doppio, .box-doppio-lista").each(function(index, el) {

      var $container = $(el).find(".col-img");
      var imgUrl = $container.find("> img").prop("src");
      $container.find("> img").css('opacity', '0');

      if(imgUrl) {
        $container
        .css("background-image", "url(" + imgUrl + ")")
        .css("background-size", "cover")
        .css("background-position", "50% 50%");
      }

    });

    $(".sezione.thumbnails .thumbnail").each(function(index, el) {

      var $container = $(el);
      var imgUrl = $container.find("img").prop("src");
      $container.find("img").css('opacity', '0');

      if(imgUrl) {
        $container
        .css("background-image", "url(" + imgUrl + ")")
        .css("background-size", "cover")
        .css("background-position", "50% 50%");
      }

    });

    $(".box-doppio-gallery .slick-slide").each(function(index, el) {

      var $container = $(el);
      var imgUrl = $container.find("img").prop("src");
      $container.find("img").css('opacity', '0');

      if(imgUrl) {
        $container
        .css("background-image", "url(" + imgUrl + ")")
        .css("background-size", "cover")
        .css("background-position", "50% 50%");
      }

    });

    $(".box-quadruplo .thumbnail").each(function(index, el) {

      var $container = $(el);
      var imgUrl = $container.find("img").prop("src");
      $container.find("img").css('opacity', '0');

      if(imgUrl) {
        $container
        .css("background-image", "url(" + imgUrl + ")")
        .css("background-size", "cover")
        .css("background-position", "50% 50%");
      }

    });

    $(".box-alternati .col-img").each(function(index, el) {

      var $container = $(el);
      var imgUrl = $container.find("img").prop("src");
      $container.find("img").css('opacity', '0');

      if(imgUrl) {
        $container
        .css("background-image", "url(" + imgUrl + ")")
        .css("background-size", "cover")
        .css("background-position", "50% 50%");
      }

    });

    $(".sezione.contatti .col-img").each(function(index, el) {

      var $container = $(el);
      var imgUrl = $container.find("img").prop("src");
      $container.find("img").css('opacity', '0');

      if(imgUrl) {
        $container
        .css("background-image", "url(" + imgUrl + ")")
        .css("background-size", "cover")
        .css("background-position", "50% 50%");
      }

    });

    $(".sezione.top-blog>.row .thumbnail").each(function(index, el) {

      var $container = $(el);
      var imgUrl = $container.find("img").prop("src");
      $container.find("img").css('opacity', '0');

      if(imgUrl) {
        $container
        .css("background-image", "url(" + imgUrl + ")")
        .css("background-size", "cover")
        .css("background-position", "50% 50%");
      }

    });

    $(".sezione.main-blog .left-blog .col-lg-8 .copertina").each(function(index, el) {

      var $container = $(el);
      var imgUrl = $container.find("img").prop("src");
      $container.find("img").css('opacity', '0');

      if(imgUrl) {
        $container
        .css("background-image", "url(" + imgUrl + ")")
        .css("background-size", "cover")
        .css("background-position", "50% 50%");
      }

    });

  }

});