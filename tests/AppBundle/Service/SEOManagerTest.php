<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Webtekteam
 */

namespace Tests\AppBundle\Service;

use AppBundle\Service\SEOManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @coversNothing
 */
class SEOManagerTest extends KernelTestCase
{
    /** @var SEOManager $service */
    private $service;

    /** @var ObjectManager $em */
    private $em;

    protected function setUp()
    {
        self::bootKernel();

        $container = static::$kernel->getContainer();

        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->service = $container->get('app.seo-manager');
    }

    public function testRun()
    {
        $home = $this->em->getRepository('AppBundle:Page')
            ->findOneByTemplate('home');

        $home_result = [
            'alternate' => [
                'en' => '/en',
                'it' => '/',
                'de' => '/de',
                'fr' => '/fr',
                'x-default' => '/en',
            ],
            'description' => 'Meta description della home',
            'robots' => 'index,follow',
            'title' => 'Meta title della home',
        ];

        $res = $this->service->run($home);
        $this->assertSame($home_result, $res);
    }
}
