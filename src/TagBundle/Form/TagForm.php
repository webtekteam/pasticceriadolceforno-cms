<?php
// 09/03/17, 15.21
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace TagBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Form\TypeExtension\BetterVichUploaderTypeExtension;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TagBundle\Entity\Tag;

class TagForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /**
         * @var $Tag Tag
         */
        $Tag = $builder->getData();

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'tag.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        $builder->add(
            'valore',
            TextType::class,
            [
                'label' => 'tag.labels.valore',
                'help' => 'tag.help.valore',
            ]
        );

        $builder->add(
            'imageFile',
            BetterVichUploaderTypeExtension::class,
            [
                'label' => 'Icona',
                'allow_delete' => true,
                'download_uri' => true,
                'required' => false,
            ]
        );


        $builder->add(
            'categoria',
            EntityType::class,
            [
                'label' => 'tag.labels.categoria',
                'class' => 'TagBundle\Entity\CategoriaTag',
            ]
        );


        $fields = [
            'nome' => [
                'label' => 'tag.labels.titolo',
                'required' => true,
                'attr' => ['class' => 'titolo'],
            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
                'label' => false,
            ]
        );

    }


    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Tag::class,
                'allow_extra_fields' => true,
                'langs' => ['it' => 'Italiano'],
            ]
        );

    }

}