<?php
// 09/03/17, 14.50
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace TagBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CategoriaTagRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('c')
            ->andWhere('c.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

    function findByName($nome, $locale = 'it')
    {

        $qb = $this->createQueryBuilder('cat')
            ->leftJoin(
                'TagBundle\Entity\CategoriaTagTranslation',
                'ct',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'ct.translatable = cat.id AND ct.locale = :locale'
            )->setParameter('locale', $locale);

        if (is_array($nome)) {
            $qb->where('ct.nome IN (:nomi)')
                ->setParameter('nomi', $nome);
        } else {
            $qb->where('ct.nome = :nome')
                ->setParameter('nome', $nome);
        }

        return $qb->getQuery()->getResult();


    }

}