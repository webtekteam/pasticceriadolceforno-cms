<?php
// 26/09/17, 10.31
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Webtek\EcommerceBundle\Entity\Product;

class WishlistAddEvent extends Event
{

    const NAME = 'whislist.add';
    /**
     * @var Product
     */
    private $Product;

    public function __construct(Product $Product)
    {

        $this->Product = $Product;
    }

    public function getProduct()
    {

        return $this->Product;

    }


}