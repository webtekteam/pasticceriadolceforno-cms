var modulo = 'um';

$(function() {

  /** Datatable della pagina di elenco */
  if ($('#datatable').length) {

    var cols = [];

    var col = {
      'title': 'Modifica',
      'className': 'dt0 dt-body-center',
      'searchable': false,
    };
    cols.push(col);

    col = {
      'title': 'Label',
      'className': 'dt1',
      searchable: true,
      data: 'label',
    };

    cols.push(col);

    col = {
      'className': 'dt2 dt-body-center',
      'searchable': false,
    };

    // placeholder cancellazione
    cols.push(col);

    var columnDefs = [];

    var columnDef = {
      targets: 0,
      searchable: false,
      orderable: false,
      className: 'dt-body-center',
      render: function(data, type, full, meta) {
        var toString = full.titolo;
        return '<a href="/admin/' + modulo + '/edit/' + full.id +
            '" title="Modifica record: ' + toString +
            '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
      },
    };

    columnDefs.push(columnDef);

    columnDef = {
      targets: cols.length - 1,
      searchable: false,
      className: 'dt-body-center',
      orderable: false,
      render: function(data, type, full, meta) {
        var toString = full.codice;
        if (full.deleted) {
          var title = _('Recupera:');
          var ret = '<a href="/admin/' + modulo + '/restore/' + full.id +
              '" class="btn btn-success btn-xs btn-restore';
          ret += '" title="' + title + toString + '" data-title="' + toString +
              '"><i class="fa fa-repeat"></i></a>';
          var title = _('Elimina definitivamente: ');
          ret += '<a href="/admin/' + modulo + '/delete/' + full.id +
              '/1" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString +
              '"><i class="fa fa-trash"></i></a>';
        } else {
          var title = 'Elimina: ';
          var ret = '<a href="/admin/' + modulo + '/delete/' + full.id +
              '" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString +
              '"><i class="fa fa-trash"></i></a>';
        }
        return ret;
      },
    };

    columnDefs.push(columnDef);

    $('#datatable').dataTable({
      ajax: {
        'url': '/admin/' + modulo + '/json',
      },
      aaSorting: [[2, 'asc']],
      columns: cols,
      columnDefs: columnDefs,
      createdRow: function(row, data, index) {
        if (data.deleted) {
          $(row).addClass('danger');
        }
      },
    });

  }

});

/** FINE document.ready **/

/**
 * Data una textarea istanzia il CKeditor e ne aggiorna l'altezza in base al contenitore in cui è inserita
 * @param textarea
 */
function attivaCkEditor(textarea) {

  var id = textarea.id;

  if (typeof(CKEDITOR.instances[id]) == 'undefined') {

    CKEDITOR.replace(textarea, {
      customConfig: '/bundles/app/admin/js/pages_ckeditor_' +
      $body.data('seo') + '.js',
    });

  }

}
