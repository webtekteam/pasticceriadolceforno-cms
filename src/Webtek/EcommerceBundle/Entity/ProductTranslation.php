<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use AppBundle\Traits\Seo;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\ProductRepository")
 * @ORM\EntityListeners({"Webtek\EcommerceBundle\EntityListener\ProductTranslationListener"})
 * @ORM\Table(name="product_translations")
 */
class ProductTranslation
{

    use Seo;
    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titolo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titolo_breve;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sottotitolo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $summary;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $testo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $dati_aggiuntivi;

    /**
     * @return mixed
     */
    public function getTitolo()
    {

        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo)
    {

        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getSummary()
    {

        return $this->summary;
    }

    /**
     * @param mixed $summary
     */
    public function setSummary($summary)
    {

        $this->summary = $summary;
    }

    /**
     * @return mixed
     */
    public function getTesto()
    {

        return $this->testo;
    }

    /**
     * @param mixed $testo
     */
    public function setTesto($testo)
    {

        $this->testo = $testo;
    }

    /**
     * @return mixed
     */
    public function getSottotitolo()
    {

        return $this->sottotitolo;
    }

    /**
     * @param mixed $sottotitolo
     */
    public function setSottotitolo($sottotitolo)
    {

        $this->sottotitolo = $sottotitolo;
    }

    /**
     * @return mixed
     */
    public function getDatiAggiuntivi()
    {

        return $this->dati_aggiuntivi;
    }

    /**
     * @param mixed $dati_aggiuntivi
     */
    public function setDatiAggiuntivi($dati_aggiuntivi)
    {

        $this->dati_aggiuntivi = $dati_aggiuntivi;
    }

    /**
     * @return mixed
     */
    public function getTitoloBreve()
    {

        return $this->titolo_breve;
    }

    /**
     * @param mixed $titolo_breve
     */
    public function setTitoloBreve($titolo_breve)
    {

        $this->titolo_breve = $titolo_breve;
    }


}

