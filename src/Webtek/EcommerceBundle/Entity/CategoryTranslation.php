<?php
// 06/02/17, 11.27
// @author : Gabriele Colombera - gabricom <gabricom-kun@live.it>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\CategoryRepository")
 * @Gedmo\Loggable
 * @ORM\EntityListeners({"Webtek\EcommerceBundle\EntityListener\CategoryTranslationListener"})
 * @ORM\Table(name="ecommerce_products_category_translations")
 */
class CategoryTranslation
{

    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @Gedmo\Versioned
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=128)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sottotitolo;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    private $descrizione;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $metaTitle;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $metaDescription;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $slug;


    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return CategoryTranslation
     */
    public function setNome($nome)
    {

        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {

        return $this->nome;
    }

    /**
     * Set descrizione
     *
     * @param string $descrizione
     *
     * @return CategoryTranslation
     */
    public function setDescrizione($descrizione)
    {

        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * Get descrizione
     *
     * @return string
     */
    public function getDescrizione()
    {

        return $this->descrizione;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     *
     * @return CategoryTranslation
     */
    public function setMetaTitle($metaTitle)
    {

        $this->metaTitle = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string
     */
    public function getMetaTitle()
    {

        return $this->metaTitle;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return CategoryTranslation
     */
    public function setMetaDescription($metaDescription)
    {

        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {

        return $this->metaDescription;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CategoryTranslation
     */
    public function setSlug($slug)
    {

        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {

        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getSottotitolo()
    {

        return $this->sottotitolo;
    }

    /**
     * @param mixed $sottotitolo
     */
    public function setSottotitolo($sottotitolo)
    {

        $this->sottotitolo = $sottotitolo;
    }


}
