<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\PrezzoRepository")
 * @ORM\Table(name="prezzo")
 */
class Prezzo
{

    use ORMBehaviours\Timestampable\Timestampable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal",  precision=12, scale=6)
     */
    private $valore;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Listino")
     * @ORM\JoinColumn(name="listino_id", referencedColumnName="id")
     */
    private $listino;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $prodotto;

    public function __toString()
    {

        return (string)$this->getId();
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getValore()
    {

        return $this->valore;
    }

    /**
     * @param mixed $valore
     */
    public function setValore($valore)
    {

        $this->valore = $valore;
    }

    /**
     * @return mixed
     */
    public function getListino()
    {

        return $this->listino;
    }

    /**
     * @param mixed $listino
     */
    public function setListino($listino)
    {

        $this->listino = $listino;
    }

    /**
     * @return mixed
     */
    public function getProdotto()
    {

        return $this->prodotto;
    }

    /**
     * @param mixed $prodotto
     */
    public function setProdotto($prodotto)
    {

        $this->prodotto = $prodotto;
    }


}
