<?php
// 07/06/17, 11.01
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Webtek\EcommerceBundle\Entity\Coupon;
use Webtek\EcommerceBundle\Entity\Product;
use AnagraficaBundle\Entity\Anagrafica;


class OrderRepository extends EntityRepository
{
    public function checkVenditaProdotto(Product $Product, Anagrafica $Anagrafica)
    {
        return $this->createQueryBuilder('ordine')
            ->join("ordine.anagraficaId", "anagrafica")
            ->join("ordine.records", "records")
            ->andWhere("anagrafica = :anagrafica")
            ->andWhere("records.product = :product")
            ->setParameter("anagrafica", $Anagrafica)
            ->setParameter("product", $Product)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();
    }

    public function contaCoupons(Coupon $coupon)
    {

        $qb = $this->createQueryBuilder('c')->andWhere('c.coupon = :coupon')->setParameter('coupon', $coupon)->select(
            'count(c.id)'
        );

        return $qb->getQuery()->getSingleScalarResult();

    }

}