<?php
// 24/04/17, 10.38
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ProductTypeRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('brands')
            ->andWhere('brands.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

    function countAllNotDeleted()
    {

        $qb = $this->createQueryBuilder('prdtype');

        $qb->select($qb->expr()->count('prdtype'))
            ->where('prdtype.deletedAt is NULL');

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }

}
