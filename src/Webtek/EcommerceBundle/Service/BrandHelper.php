<?php
// 24/04/17, 10.43
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Service;


use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\Brand;
use Webtek\EcommerceBundle\Entity\BrandTranslation;

class BrandHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * BrandsHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }


    public function getList($deleted = false)
    {

        if ($deleted) {
            $Brands = $this->entityManager->getRepository('WebtekEcommerceBundle:Brand')->findAll();
        } else {
            $Brands = $this->entityManager->getRepository('WebtekEcommerceBundle:Brand')->findAllNotDeleted();
        }

        $records = [];

        foreach ($Brands as $Brand) {


            /**
             * @var $Brand Brand;
             */

            $record = [];
            $record['id'] = $Brand->getId();
            $record['titolo'] = $Brand->translate()->getTitolo();
            $record['deleted'] = $Brand->isDeleted();
            $record['isEnabled'] = $Brand->getIsEnabled();
            $record['createdAt'] = $Brand->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Brand->getUpdatedAt()->format('d/m/Y H:i:s');


            $records[] = $record;
        }

        return $records;

    }

    public function getMetaDescription(BrandTranslation $brandTranslation)
    {

        $text = strip_tags($brandTranslation->getTesto());

        $text = substr($text, 0, 150);

        return html_entity_decode($text, ENT_QUOTES, 'UTF-8');

    }


}
