<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace Webtek\EcommerceBundle\Service;

use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\UM;

class UMHelper
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * UMHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Ritorna un array semplificato degli elementi presenti in database
     * per essere mostrato in datatable.
     *
     * @param bool $deleted
     *
     * @return array
     */
    public function getList($deleted = false)
    {
        if ($deleted) {
            $UMS = $this->entityManager->getRepository('WebtekEcommerceBundle:UM')->findAll();
        } else {
            $UMS = $this->entityManager->getRepository('WebtekEcommerceBundle:UM')->findAllNotDeleted();
        }
        $records = [];
        foreach ($UMS as $UM) {
            /**
             * @var UM
             */
            $record = [];
            $record['id'] = $UM->getId();
            $record['label'] = $UM->getLabel();
            $record['deleted'] = $UM->isDeleted();
            $record['createdAt'] = $UM->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $UM->getUpdatedAt()->format('d/m/Y H:i:s');
            $records[] = $record;
        }

        return $records;
    }
}
