<?php

namespace Webtek\EcommerceBundle\Service;


use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\ProductType;

class ProductTypeHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * ProductTypesHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $ProductTypes = $this->entityManager->getRepository('WebtekEcommerceBundle:ProductType')->findAll();
        } else {
            $ProductTypes = $this->entityManager->getRepository('WebtekEcommerceBundle:ProductType')->findAllNotDeleted();
        }

        $records = [];

        foreach ($ProductTypes as $ProductType) {

            $gruppi = $ProductType->getGruppiAttributi();

            $groups = [];

            foreach ($gruppi as $gruppo) {
                $groups[] = (string)$gruppo;
            }

            /**
             * @var $ProductType ProductType;
             */

            $record = [];
            $record['id'] = $ProductType->getId();
            $record['nome'] = $ProductType->getNome();
            $record['gruppi'] = $groups;
            $record['deleted'] = $ProductType->isDeleted();
            $record['createdAt'] = $ProductType->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $ProductType->getUpdatedAt()->format('d/m/Y H:i:s');

            $records[] = $record;

        }

        return $records;

    }


}