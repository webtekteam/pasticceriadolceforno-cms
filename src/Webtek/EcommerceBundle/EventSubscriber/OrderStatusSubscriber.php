<?php
/**
 * Created by PhpStorm.
 * User: gabricom
 * Date: 03/08/17
 * Time: 16.57
 */

namespace Webtek\EcommerceBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Webtek\EcommerceBundle\Event\OrderStatusChangeEvent;
use Webtek\EcommerceBundle\Service\OrderMailer;

class OrderStatusSubscriber implements EventSubscriberInterface
{

    private $orderMailer;

    public function __construct(OrderMailer $orderMailer)
    {
        $this->orderMailer = $orderMailer;
    }


    public static function getSubscribedEvents()
    {
        return array(
            OrderStatusChangeEvent::NAME => 'onStatusChange',
        );
    }

    public function onStatusChange(OrderStatusChangeEvent $event)
    {
        $this->orderMailer->generateEmail($event->getOrder());
    }
}