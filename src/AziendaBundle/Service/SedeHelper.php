<?php
// 22/06/17, 12.01
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AziendaBundle\Service;

use Doctrine\ORM\EntityManager;
use AziendaBundle\Entity\Sede;

class SedeHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $Sedi = $this->entityManager->getRepository(Sede::class)->findAll();
        } else {
            $Sedi = $this->entityManager->getRepository(Sede::class)->findAllNotDeleted();
        }

        $records = [];

        foreach ($Sedi as $Sede) {
            /**
             * @var $Sede Sede;
             */
            $record = [];
            $record['id'] = $Sede->getId();
            $record['nome'] = $Sede->getNome();
            $record['slug'] = $Sede->translate()->getSlug();
            $record['deleted'] = $Sede->isDeleted();
            $record['isEnabled'] = $Sede->getIsEnabled();
            $record['createdAt'] = $Sede->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Sede->getUpdatedAt()->format('d/m/Y H:i:s');

            $records[] = $record;
        }

        return $records;

    }


}