<?php
// 29/06/17, 11.57
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AziendaBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity()
 * @ORM\Table(name="orari")
 * @Vich\Uploadable()
 */
class Orario
{

    use Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $etichetta;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $dalle;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $alle;

    /**
     * @ORM\Column(type="string")
     */
    private $tipo;

    /**
     * @ORM\Column(type="integer")
     */
    private $sort;

    /**
     * @ORM\ManyToOne(targetEntity="AziendaBundle\Entity\Department")
     */
    private $department;

    /**
     * @return mixed
     */
    public function getEtichetta()
    {

        return $this->etichetta;
    }

    /**
     * @param mixed $etichetta
     */
    public function setEtichetta($etichetta)
    {

        $this->etichetta = $etichetta;
    }

    /**
     * @return mixed
     */
    public function getDalle()
    {

        return $this->dalle;
    }

    /**
     * @param mixed $dalle
     */
    public function setDalle($dalle)
    {

        $this->dalle = $dalle;
    }

    /**
     * @return mixed
     */
    public function getAlle()
    {

        return $this->alle;
    }

    /**
     * @param mixed $alle
     */
    public function setAlle($alle)
    {

        $this->alle = $alle;
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {

        return $this->department;
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department)
    {

        $this->department = $department;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {

        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {

        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {

        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort)
    {

        $this->sort = $sort;
    }


}