<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Behat\AppBundle\Admin\Context;

use Behat\AppBundle\Admin\Page\PanelPage;
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\RawMinkContext;

class AdminContext extends RawMinkContext implements Context
{
    /**
     * @var PanelPage
     */
    private $panelPage;

    /**
     * AdminContext constructor.
     */
    public function __construct(PanelPage $panelPage)
    {
        $this->panelPage = $panelPage;
    }

    /**
     * @Given I open :name menu
     *
     * @param mixed $name
     */
    public function iOpenMenu($name)
    {
        $menu = $this->panelPage->findXpathDataAttr($name);
        $menu->click();
    }

    /**
     * @Given I wait for :name to appear
     *
     * @param mixed $name
     */
    public function iWaitForToAppear($name)
    {
        $this->panelPage->waitForXpathToAppear($name);
    }

    /**
     * @Then I wait for table to load
     */
    public function iWaitForTableToLoad()
    {
        $this->panelPage->waitDataTable();
    }

    /**
     * @Given I edit the :label row
     *
     * @param mixed $label
     */
    public function iEditTheRow($label)
    {
        $this->panelPage->editRow($label);
    }

    /**
     * @Given I delete the :label row
     *
     * @param mixed $label
     */
    public function iDeleteTheRow($label)
    {
        $this->panelPage->deleteRow($label);
    }

    /**
     * @Given I wait for confirmation modal
     */
    public function iWaitForConfirmationModal()
    {
        $session = $this->getSession();
        $this->panelPage->waitConfirmation($session);
    }

    /**
     * @Given I confirm the modal
     */
    public function iConfirmTheModal()
    {
        $session = $this->getSession();
        $this->panelPage->confirmModal($session);
    }

    /**
     * @Then I restore the :label row
     *
     * @param mixed $label
     */
    public function iRestoreTheRow($label)
    {
        $this->panelPage->restoreRow($label);
    }

    /**
     * @Then I fill in data :name with :value
     *
     * @param mixed $name
     * @param mixed $value
     */
    public function iFillInDataWith($name, $value)
    {
        $this->panelPage->fillXpathDataAttr($name, $value);
    }

    /**
     * @Then I select :value from data :selector
     *
     * @param mixed $value
     * @param mixed $selector
     */
    public function iSelectFromData($value, $selector)
    {
        $this->panelPage->selectXpathDataAttr($selector, $value);
    }

    /**
     * @Given I click on :xpath
     *
     * @param mixed $xpath
     */
    public function iClickOn($xpath)
    {
        $this->panelPage->findXpathDataAttr($xpath)->click();
    }

    /**
     * @Then I search :query in datatable
     *
     * @param mixed $query
     */
    public function iSearchInDatatable($query)
    {
        $this->panelPage->fillDataTableSearch($query);
    }

    /**
     * @Then I click on :icon icon for the :label row
     *
     * @param mixed $icon
     * @param mixed $label
     */
    public function iClickOnIconForTheRow($icon, $label)
    {
        $this->panelPage->clickOnNamedIcon($icon, $label);
    }

    /**
     * @Then I image with name :name should be reachable
     *
     * @param mixed $name
     */
    public function iImageWithNameShouldBeReachable($name)
    {
        $src = $this->panelPage->getImageSRC($name);
        $this->visitPath($src);
    }

    /**
     * @Then I scrollo to :label
     *
     * @param mixed $label
     */
    public function iScrolloTo($label)
    {
        $elem = $this->panelPage->findXpathDataAttr($label);
        $function
            = '
        (function(){
          var elem = document.getElementById("%s");
          elem.scrollIntoView(false);
          $(".sf-toolbar").remove();
        })()';

        try {
            $this->getSession()->executeScript(sprintf($function, $elem->getAttribute('id')));
        } catch (\Exception $e) {
            throw new \Exception('ScrollIntoView failed');
        }
    }

    /**
     * @Then I press element :label
     *
     * @param mixed $label
     */
    public function iPressElement($label)
    {
        $this->panelPage->pressXPathButton($label);
    }
}
