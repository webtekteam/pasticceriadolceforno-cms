<?php
// 12/06/17, 16.58
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class EmailTemplateRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('emailtemplates')
            ->andWhere('emailtemplates.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }


}