<?php
// 13/01/17, 8.57
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

/**
 * Class CategorieNewsTranslationRepository
 * @package AppBundle\Repository
 */
class CategorieNewsTranslationRepository extends EntityRepository
{


    /**
     *
     * @param $slug
     * @param $locale
     * @param $id
     * @return mixed
     */
    function findBySlugButNotId($slug, $locale, $id)
    {

        $query = $this->createQueryBuilder('translation')
            ->leftJoin(
                'AppBundle\Entity\NewsCategory',
                'nc',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'nc.id = translation.translatable'
            )
            ->andWhere('translation.slug = :slug')
            ->setParameter('slug', $slug)
            ->andWhere('translation.locale = :locale')
            ->setParameter('locale', $locale)
            ->andWhere('translation.id != :id')
            ->setParameter('id', $id)
            ->andWhere('nc.deletedAt IS NULL')
            ->getQuery();

        return $query->execute();
    }

}