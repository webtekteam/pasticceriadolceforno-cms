<?php
// 09/02/17, 15.47
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;

class PathManager
{

    /**
     * @var Router
     */
    private $router;
    /**
     * @var Request
     */
    private $request;

    public function __construct(Router $router, RequestStack $requestStack)
    {

        $this->router = $router;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function generate(
        $routeName,
        $routeParams = [],
        $locale = null,
        $absolute = false,
        $debug = false
    ) {

        if (!$locale) {
            try {
                $locale = $this->request->getLocale();
            } catch (\Exception $e) {
                $this->request;
            }
        }

        $strategy = UrlGeneratorInterface::ABSOLUTE_PATH;

        if ($absolute) {
            $strategy = UrlGeneratorInterface::ABSOLUTE_URL;
        }

        if ($locale == 'it') {

            if (isset($routeParams['_locale'])) {
                unset($routeParams['_locale']);
            }

            if (!preg_match('/_it$/', $routeName)) {
                $routeName .= '_it';
            }

            $url = '#';

            if ($this->checkIfRouteExists($routeName)) {
                $url = $this->router->generate($routeName, $routeParams, $strategy);
            }

        } else {


            $url = '#';

            $routeParams['_locale'] = $locale;

            if ($this->checkIfRouteExists($routeName)) {
                $url = $this->router->generate($routeName, $routeParams, $strategy);
            }

        }

        return $url;

    }

    /**
     * @deprecated
     */
    public function generateUrl($routeName, $routeParams = [], $locale = null, $absolute = false, $debug = false)
    {

        return $this->generate($routeName, $routeParams, $locale, $absolute, $debug);


    }

    private function checkIfRouteExists($routeName)
    {

        return (null === $this->router->getRouteCollection()->get($routeName)) ? false : true;
    }


}