<?php
// 08/03/17, 10.03
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class RolesHelper
{

    private $rolesHierarchy;

    private $roles;
    /**
     * @var RoleHierarchy
     */
    private $roleHierarchy;
    private $rolesHierarchyList;
    private $reachableRoles;

    public function __construct($rolesHierarchyList, RoleHierarchy $roleHierarchy)
    {


        $this->roleHierarchy = $roleHierarchy;
        $this->rolesHierarchyList = $rolesHierarchyList;

    }

    public function getRoles()
    {

        if ($this->roles) {
            return $this->roles;
        }

        $roles = [];

        foreach ($this->rolesHierarchyList as $k => $val) {
            $roles[] = $k;
        }

        array_walk_recursive(
            $this->rolesHierarchyList,
            function ($val) use (&$roles) {

                $roles[] = $val;
            }
        );

        return $this->roles = array_unique($roles);
    }

    private function getRolesObjects()
    {

        if (!$this->roles) {

            $this->getRoles();

        }

        $rolesObjects = [];

        foreach ($this->roles as $role) {

            $rolesObjects[] = new Role($role);

        }

        return $rolesObjects;

    }

    public function getAvailableRoles()
    {

        if ($this->reachableRoles) {
            return $this->reachableRoles;
        }

        $reachableRoles = $this->roleHierarchy->getReachableRoles($this->getRolesObjects());

        $roles = [];

        foreach ($reachableRoles as $role) {
            /**
             * var $role Role
             */
            $roles[] = $role->getRole();
        }

        return $this->reachableRoles = array_unique($roles);

    }
}
