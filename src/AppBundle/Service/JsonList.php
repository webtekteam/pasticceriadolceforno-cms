<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Classe per la generazione di un elenco di elementi da dare in pasto a DataTable
 *
 * Class JsonList
 * @package AppBundle\Service
 */
class JsonList
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var string
     */
    private $entityName;

    /**
     * @var bool
     */
    private $canViewDeleted = true;

    /**
     * @var array
     */
    private $fields = [];

    /**
     * @var \ReflectionClass
     */
    private $Reflection;

    /**
     * @var array
     */
    private $rowModifiers = [];

    /**
     * @var array
     */
    private $modifiers = [];

    /**
     * @var string
     */
    private $repositoryMethod;

    /**
     * @var array
     */
    private $repositoryArguments;

    /**
     * JsonList constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->setDefaultModifiers();
    }

    /**
     * @param string $entityName
     *
     * @throws \ReflectionException
     *
     * @return JsonList
     */
    public function setEntity(String $entityName): self
    {
        $this->entityName = $entityName;
        $this->getReflection();

        return $this;
    }

    /**
     * @param string $methodName
     * @param array $repositoryArguments
     *
     * @return $this
     */
    public function setRepositoryMethod(string $methodName, array $repositoryArguments = [])
    {
        $this->repositoryMethod = $methodName;
        $this->repositoryArguments = $repositoryArguments;

        return $this;
    }

    /**
     * Imposta il parametro onlyActive, se la RepositoryClass dell'entità in esame usa il trait
     * Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletable, questo parametro verrà utilizzato
     * per decidere se mostrare solo gli elementi che hanno il campo "deletedAt" a null.
     *
     * @param bool $onlyActive
     *
     * @return JsonList
     */
    public function setCanViewDeleted(bool $canViewDelete): self
    {
        $this->canViewDeleted = $canViewDelete;

        return $this;
    }

    /**
     * Metodo per settare i campi da includere nell'array risultante.
     *
     * @param array $fields
     *
     * @return JsonList
     */
    public function setFields(array $fields): self
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @param $field
     * @param callable $function
     *
     * @return JsonList
     */
    public function setModifier($field, callable $function): self
    {
        $this->modifiers[$field] = $function;

        return $this;
    }

    /**
     * @param $field
     * @param callable $function
     *
     * @return JsonList
     */
    public function addFieldToRow($field, callable $function): self
    {
        $this->rowModifiers[$field] = $function;

        return $this;
    }

    /**
     * @throws \Exception
     *
     * @return array of elements
     */
    public function getList()
    {
        if ($this->entityName) {
            $repo = $this->entityManager->getRepository($this->entityName);
            if (!$this->repositoryMethod) {
                if (!$this->canViewDeleted && $this->hasSoftDeletable()) {
                    $items = $repo->findAllNotDeleted($this->onlyActive);
                } else {
                    $items = $repo->findAll();
                }
            } else {
                $items = call_user_func_array([$repo, $this->repositoryMethod], $this->repositoryArguments);
            }
            $accessor = PropertyAccess::createPropertyAccessor();
            if ($this->fields) {
                $rows = [];
                foreach ($items as $item) {
                    $row = [
                        'id' => $accessor->getValue($item, 'id'),
                    ];
                    foreach ($this->fields as $field) {
                        if (is_array($field)) {
                            $property = array_values($field)[0];
                            $field = array_keys($field)[0];
                        } else {
                            $property = $field;
                        }

                        try {
                            $row[$field] = $accessor->getValue($item, $property);
                        } catch (\Exception $e) {
                            $row[$field] = null;
                        }
                        if (isset($this->modifiers[$field])) {
                            $row[$field] = $this->modifiers[$field]($row[$field], $item);
                        }
                    }
                    if ($this->rowModifiers) {
                        foreach ($this->rowModifiers as $field => $function) {
                            $row[$field] = $function();
                        }
                    }
                    $rows[] = $row;
                }

                return $rows;
            }

            throw new \Exception('Non hai settato i campi da includere nell\'elenco');
        }

        throw new \Exception('Non è stata settata la classe di riferimento');
    }

    /**
     * @return bool
     */
    private function hasSoftDeletable()
    {
        $Traits = $this->Reflection->getTraits();
        $found = false;
        foreach ($Traits as $trait) {
            /* @var $trait \ReflectionClass */
            if ('Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletable' == $trait->getName()) {
                $found = true;

                break;
            }
        }

        return $found;
    }

    /**
     * @throws \ReflectionException
     */
    private function getReflection()
    {
        if (!$this->Reflection) {
            $this->Reflection = new \ReflectionClass($this->entityName);
        }
    }

    private function setDefaultModifiers()
    {
        $this->setModifier(
            'updatedAt',
            function ($value) {
                return $value->format('d/m/Y H:i:s');
            }
        );
        $this->setModifier(
            'deletedAt',
            function ($value) {
                return $value->format('d/m/Y H:i:s');
            }
        );
        $this->setModifier(
            'createdAt',
            function ($value) {
                return $value->format('d/m/Y H:i:s');
            }
        );
    }
}
