<?php
// 11/02/17, 8.13
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;


use AppBundle\Entity\Attachment;
use AppBundle\Entity\News;
use AppBundle\Entity\NewsRows;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BlogFormatter extends DataFormatter
{

    private $NewsHeader = [];

    public function getData()
    {
        // TODO: Implement getData() method.
    }

    public function extractData()
    {
        // TODO: Implement extractData() method.
    }

    public function getPiuLette()
    {

        $Repo = $this->em->getRepository('AppBundle:News');

        $News = $Repo->findMostVisited($this->locale, $this->data[$this->locale]['quante']['val']);

        $data = [];
        $data['News'] = $News;

        return $data;

    }

    public function getMonths()
    {

        $Repo = $this->em->getRepository('AppBundle:News');

        $Mesi = $Repo->getMonths($this->locale, $this->data[$this->locale]['quanti']['val']);

        $data = [];

        foreach ($Mesi as $Mese) {

            $date = new \DateTime($Mese['date']);

            $record = [];
            $record['label'] = $this->container->get('translator')->trans('cal.labels.mese_' . $date->format('n')) . ' ' . $date->format('Y');
            $record['month'] = $date->format('n') . '-' . $date->format('Y');

            $data[] = $record;

        }


        if (count($data) > 1) {
            return $data;


        }

        return false;


    }

    function getEvidenza()
    {

        $NewsCategoryAll = $this->em->getRepository('AppBundle:NewsCategory')->findBy(
            ['isEnabled' => 1],
            ['sort' => 'ASC'],
            $this->data[$this->locale]['num_categorie']['val'],
            0
        );

        $NewsHeader = [];

        foreach ($NewsCategoryAll as $newsCategory) {

            /**
             * @var $NewsHasCategory NewsHasCategory
             */
            $NewsHasCategories = $this->em->getRepository('AppBundle:NewsHasCategory')->getNews(
                $newsCategory->getId(),
                $this->locale,
                0,
                1,
                $NewsHeader
            );

            if ($NewsHasCategories) {

                $NewsHasCategory = $NewsHasCategories[0];

                $News = $NewsHasCategory->getNews();

                $NewsHeader[] = $News;

            }

        }

        if (count($NewsHeader) < 4) {

            $res = $this->em->getRepository('AppBundle:News')->findNewsButIds(
                $NewsHeader,
                $this->locale,
                0,
                $this->data[$this->locale]['num_categorie']['val'] - count($NewsHeader)
            );

            $NewsHeader = array_merge($NewsHeader, $res);

        }

        $this->NewsHeader = $NewsHeader;

        if (count($this->NewsHeader) < 4) {
            $this->NewsHeader = [];
        }

        return ['newsInEvidenza' => $this->NewsHeader];

    }

    public function getPage()
    {

        $blogData = [];
        $blogData['type'] = 'blog';
        $blogData['META'] = [];

        $next = false;
        $prev = false;

        if ($this->request->get('slug')) {

            $blogData['type'] = 'category';

            $NewsCategoryTranslation = $this->em->getRepository('AppBundle:NewsCategoryTranslation')->findOneBy(
                ['slug' => $this->request->get('slug'), 'locale' => $this->locale]
            );

            if (!$NewsCategoryTranslation) {
                throw new NotFoundHttpException("Pagina non trovata");
            }

            $NewsCategory = $NewsCategoryTranslation->getTranslatable();

            if (!$NewsCategory->getIsEnabled()) {
                throw new NotFoundHttpException("Pagina non trovata");
            }

            $query = $this->em->getRepository('AppBundle:NewsHasCategory')->getNewsQuery(
                $NewsCategory->getId(),
                $this->locale
            );

            $paginator = $this->container->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query,
                $this->request->query->getInt('page', 1),
                $this->container->getParameter('generali')['elementi_pagina_paginatore']
            );

            $paginationData = $pagination->getPaginationData();

            if ($paginationData['current'] > 1) {
                $prev = $this->container->get('app.path_manager')->generateUrl(
                    'blog_category',
                    ['slug' => $NewsCategoryTranslation->getSlug(), 'page' => $paginationData['current'] - 1]
                );
            }
            if ($paginationData['current'] < $paginationData['last']) {
                $next = $this->container->get('app.path_manager')->generateUrl(
                    'blog_category',
                    ['slug' => $NewsCategoryTranslation->getSlug(), 'page' => $paginationData['current'] + 1]
                );
            }

            $page = 1;

            if ($paginationData['current']) {
                $page = $paginationData['current'];
            }


            $blogData['META']['title'] = $NewsCategory->translate($this->request->getLocale())->getMetaTitle();
            $blogData['META']['description'] = $NewsCategory->translate(
                $this->request->getLocale()
            )->getMetaDescription();

            $blogData['META']['next'] = $next;
            $blogData['META']['prev'] = $prev;

        } else {

            $mese = null;

            if ($this->request->query->has('mese') && preg_match(
                    '/^\d{1,2}\-\d{4}$/',
                    $this->request->query->get('mese')
                )
            ) {
                $mese = $this->request->query->get('mese');
            }

            // estraggo il resto delle news
            $QB = $this->em->getRepository('AppBundle:News')->findNewsButIdsQB($this->NewsHeader, $this->locale, $mese);

            $paginator = $this->container->get('knp_paginator');
            $pagination = $paginator->paginate(
                $QB,
                $this->request->query->getInt('page', 1),
                $this->container->getParameter('generali')['elementi_pagina_paginatore']
            );

            $paginationData = $pagination->getPaginationData();


            if ($paginationData['current'] > 1) {
                $prev = $this->container->get('app.path_manager')->generateUrl(
                    'blog',
                    ['page' => $paginationData['current'] - 1, '_locale' => $this->request->getLocale()]
                );
            }
            if ($paginationData['current'] < $paginationData['last']) {
                $next = $this->container->get('app.path_manager')->generateUrl(
                    'blog',
                    ['page' => $paginationData['current'] + 1, '_locale' => $this->request->getLocale()]
                );
            }

            $blogData['META']['next'] = $next;
            $blogData['META']['prev'] = $prev;

        }

        $blogData['pagination'] = $pagination;

        return $blogData;

    }


    public function getNews()
    {

        return ['News' => $this->AdditionalData['Entity']];

    }

    public function getBlogPost()
    {


        if (isset($this->AdditionalData['Entity'])) {

            /**
             * @var $News News
             */
            $News = $this->AdditionalData['Entity'];

            $primaRiga = false;
            if ($this->data[$this->locale]['da_riga']['val']) {
                $primaRiga = intval($this->data[$this->locale]['da_riga']['val'] - 1);
            }
            $length = null;

            if (is_numeric(
                    $this->data[$this->locale]['a_riga']['val']
                ) && $this->data[$this->locale]['a_riga']['val'] > $primaRiga
            ) {
                $length = $this->data[$this->locale]['a_riga']['val'] - $primaRiga;
            }

            $rows = [];

            $out = '';

            foreach ($News->getNewsRows() as $newsRow) {
                /**
                 * @var $newsRow NewsRows
                 */
                if ($newsRow->getLocale() == $this->locale) {
                    $rows[] = $newsRow;
                }
            }

            if ($primaRiga !== false) {
                $rows = array_slice($rows, $primaRiga, $length);
            }

            $data = [];
            $data['title'] = $News->translate($this->locale)->getTitolo();
            $data['subtitle'] = $News->translate()->getSottotitolo();
            $data['commenti'] = $this->em->getRepository('AppBundle:Commento')->getCommentiCountForNews($News);
            $data['visite'] = $News->translate($this->locale)->getVisits();
            $data['rows'] = $rows;
            $data['full-news'] = $News;

        }

        return $data;

    }

    /**
     * @deprecated
     */
    public function getGallery()
    {

        $vich_uploader = $this->container->get('vich_uploader.templating.helper.uploader_helper');


        $data = [];
        $data['images'] = [];

        if (isset($this->AdditionalData['Entity'])) {
            /**
             * @var $News News
             */
            $News = $this->AdditionalData['Entity'];


            $Attachments = $News->getAttachments();

            foreach ($Attachments as $Attachment) {

                /**
                 * @var $Attachment Attachment
                 */

                $item = [];
                $item['alt'] = $Attachment->translate($this->locale)->getAlt();
                $item['src'] = $vich_uploader->asset($Attachment, 'file');

                $data['images'][] = $item;

            }

        }


        return $data;

    }

    public function getLastBlogPost()
    {

        $Repo = $this->em->getRepository('AppBundle:News');

        $News = $Repo->findNewsButIds([], $this->locale, 0, $this->data[$this->locale]['quante']['val']);

        $data = [];
        $data['News'] = $News;

        return $data;

    }

    public function getSearchResults()
    {

        $blogData = [];
        $blogData['type'] = 'blog';
        $blogData['META'] = [];

        $next = false;
        $prev = false;

        $paginator = $this->container->get('knp_paginator');
        if ($this->AdditionalData["news"] === null) {
            $this->AdditionalData["news"] = [];
        }

        $pagination = $paginator->paginate(
            $this->AdditionalData["news"],
            $this->request->query->getInt('page', 1),
            $this->container->getParameter('generali')['elementi_pagina_paginatore']
        );

        $paginationData = $pagination->getPaginationData();

        if ($paginationData['current'] > 1) {
            $prev = $this->container->get('app.path_manager')->generateUrl(
                'blog',
                [
                    'page' => $paginationData['current'] - 1,
                    '_locale' => $this->request->getLocale(),
                ]
            );
        }

        if ($paginationData['current'] < $paginationData['last']) {
            $next = $this->container->get('app.path_manager')->generateUrl(
                'blog',
                [
                    'page' => $paginationData['current'] + 1,
                    '_locale' => $this->request->getLocale(),
                ]
            );
        }

        $blogData['META']['next'] = $next;
        $blogData['META']['prev'] = $prev;

        $blogData['pagination'] = $pagination;

        return $blogData;

    }

}