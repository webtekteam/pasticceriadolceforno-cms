<?php
// 17/01/17, 14.12
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MaxCategories extends Constraint
{

    public $message_field = 'news_categories.messages.non_attivabile';

    public $message = 'news_categories.messages.non_attivabile_extended';

    public function getTargets()
    {

        return self::CLASS_CONSTRAINT;
    }

}