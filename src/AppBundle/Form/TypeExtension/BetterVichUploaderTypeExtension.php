<?php
// 13/03/17, 9.41
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form\TypeExtension;


use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Handler\UploadHandler;
use Vich\UploaderBundle\Storage\StorageInterface;

class BetterVichUploaderTypeExtension extends VichFileType
{

    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var UploadHandler
     */
    protected $handler;


    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {

        parent::buildView($view, $form, $options);

        $view->vars['basename'] = '';

        if ($view->vars['object'] && isset($view->vars['download_uri']) && $view->vars['download_uri']) {
            $view->vars['basename'] = basename($view->vars['download_uri']);
        }

    }


    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {

        return 'better_vich_image';
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'allow_delete' => true,
                'download_uri' => true,
                'image_uri' => false,
                'delete_label' => false,
                'download_label' => 'download',
            ]
        );
    }

}