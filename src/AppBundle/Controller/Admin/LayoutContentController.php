<?php
// 23/01/17, 11.27
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Template;
use AppBundle\Service\Slugger;
use Cocur\Slugify\Slugify;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_LAYOUT_CONTENT')")
 */
class LayoutContentController extends Controller
{

    /**
     * @Route("/layoutcontent", name="layoutcontent")
     */
    public function listAction()
    {

        return $this->render('admin/layoutcontent/list.html.twig');

    }

    /**
     * @Route("/layoutcontent/json", name="layoutcontent_list_json")
     */
    public function listJson()
    {

        $em = $this->getDoctrine()->getManager();

        $Templates = $em->getRepository('AppBundle:Template')->findBy(
            ['layout' => $this->getParameter('generali')['layout']]
        );

        $retData = [];

        $templates = [];

        foreach ($Templates as $Template) {
            /**
             * @var $Template Template;
             */
            if ($this->isGranted($Template->getRequiredRole())) {

                $record = [];
                $record['id'] = $Template->getId();
                $record['module'] = $Template->getModule();
                $record['position'] = $Template->getPosition();
                $record['codice'] = $Template->getCodice();
                $record['container'] = $Template->getContainer();
                $record['template'] = $Template->getTemplate();
                if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                    $record['requiredRole'] = $Template->getRequiredRole();
                }
                $record['type'] = $Template->getType();

                $templates[] = $record;
            }
        }

        $retData['data'] = $templates;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/layoutcontent/recompile-css/{id}", name="layoutcontent_recompile")
     */
    public function recompileAction(Request $request, Template $template)
    {

        $vars = $request->request->get('vars');

        $TemplateLoader = $this->get('app.template_loader');

        $Languages = $this->get('app.languages');

        $widget = $TemplateLoader->getWidget($template, $Languages->getActiveLanguages(), 'admin');

        $TemplateLoader->compileStyle($widget, $vars);

        $return['result'] = 1;
        $return['css'] = $widget['css'];

        return new JsonResponse($return);

    }

    /**
     * @Route("/layoutcontent/edit/{id}", name="layoutcontent_edit")
     */
    public function editAction(Request $request, Template $template)
    {

        $TemplateLoader = $this->get('app.template_loader');

        $Trans = $this->get('translator');

        $Languages = $this->get('app.languages');

        $widget = $TemplateLoader->getWidget($template, $Languages->getActiveLanguages(), 'admin');

        $oldConfig = $template->getConfig();

        $scssVars = null;

        if (isset($oldConfig['scssVars'])) {
            $scssVars = $oldConfig['scssVars'];
        }

        $TemplateLoader->compileStyle($widget, $scssVars);

        $scssFile = $widget['directory'] . '/' . $widget['name'] . '.scss';
        $twigFile = $widget['directory'] . '/' . $widget['name'] . '.twig';
        $jsFile = '/public/js/' . $this->getParameter('generali')['layout'] . '/modules.js';

        $scssVariables = [];
        $scss = false;
        $twig = false;
        $colors = false;
        $textsize = false;
        $textweight = false;

        $moduleAssets = [];

        if ($this->isGranted('ROLE_MANAGE_COLORS') && file_exists($scssFile)) {

            $assetsLayoutFile = $this->get('kernel')->getRootDir() . '/Resources/views/public/layouts/' . $this->getParameter('generali')['layout'] . '/assets.json';

            $moduleAssets = $widget['assets'];

            if ($assetsLayoutFile) {
                $assets = json_decode(file_get_contents($assetsLayoutFile), true);
            }

            if (!isset($moduleAssets['css'])) {
                $moduleAssets['css'] = [];
            }
            if (!isset($moduleAssets['js'])) {
                $moduleAssets['js'] = [];
            }

            $moduleAssets['css'] = array_merge($assets['css'], $moduleAssets['css']);
            $moduleAssets['js'] = array_merge($assets['js'], $moduleAssets['js']);

            $moduleAssets['js'][] = str_replace($this->get('kernel')->getRootDir(), '', $jsFile);

            $scss = file_get_contents($scssFile);
            $twig = file_get_contents($twigFile);
            $scssVariables = $this->get('app.simple_sass_parser')->getVariables($scssFile);


            $colors = [];
            $textsize = [];
            $textweight = [];


            $variables = $this->get('kernel')->getRootDir() . '/Resources/views/public/layouts/' . $this->getParameter(
                    'generali'
                )['layout'] . '/styles/common/_variables.scss';

            if (!file_exists($variables)) {
                throw new \Exception(
                    'Il file delle variabili non esiste per il layout ' . $this->getParameter('generali')['layout']
                );
            }

            $variables = file($variables);

            foreach ($variables as $variable) {
                $re = '/(\$color[a-zA-Z0-9_\-]+)\s*\:\s*([^;]+)/';
                if (preg_match($re, $variable, $m)) {
                    $colors[$m[1]] = $m[2];
                }
                $re = '/(\$textsize[a-zA-Z0-9_\-]+)\s*\:\s*([^;]+)/';
                if (preg_match($re, $variable, $m)) {
                    $textsize[$m[1]] = $m[2];
                }
                $re = '/(\$fontweight[a-zA-Z0-9_\-]+)\s*\:\s*([^;]+)/';
                if (preg_match($re, $variable, $m)) {
                    $textweight[$m[1]] = $m[2];
                }
            }


        }

        if ($request->isMethod('POST')) {

            $error = false;

            $config = [];

            foreach ($Languages->getActiveLanguages() as $shortLocale => $longLocale) {
                $langData = $request->request->get($shortLocale);
                if ($langData) {
                    foreach ($langData as $key => $value) {

                        switch ($widget['meta']['config'][$key]['type']) {
                            case 'link':
                                if (isset($widget['meta']['config'][$key]['allow_add']) && $widget['meta']['config'][$key]['allow_add']) {
                                    $check = true;
                                } else {
                                    $check = $value['link'];
                                }
                                break;
                            case 'image':
                            case 'backgroundImage':
                            case 'file':
                                $check = true;
                                break;
                            default:
                                if (isset($widget['meta']['config'][$key]['allow_add']) && $widget['meta']['config'][$key]['allow_add']) {
                                    $check = true;
                                } else {
                                    $check = $value['val'];
                                }
                        }

                        if ($check === '' && (isset($widget['meta']['config'][$key]['mandatory']) && $widget['meta']['config'][$key]['mandatory'])) {
                            $error = true;
                            $this->addFlash(
                                'error',
                                sprintf(
                                    $Trans->trans('layoutcontent.messages.mandatory'),
                                    $widget['meta']['config'][$key]['label'],
                                    $longLocale
                                )
                            );
                        } else {
                            $config[$shortLocale][$key] = $value;
                        }
                    }

                }
            }

            if (file_exists($scssFile)) {
                if ($this->isGranted('ROLE_MANAGE_COLORS')) {
                    $config['scssVars'] = $request->request->get('scssVars');
                } elseif (isset($oldConfig['scssVars'])) {
                    $config['scssVars'] = $oldConfig['scssVars'];
                }
            }

            if (!$error) {

                if ($this->isGranted('ROLE_SUPER_ADMIN')) {
                    if (in_array(
                        $request->request->get('requiredRole'),
                        ['ROLE_SUPER_ADMIN', 'ROLE_USER', 'ROLE_ADMIN']
                    )) {
                        $template->setRequiredRole($request->request->get('requiredRole'));
                    }
                }

                $template->setConfig($config);

                $em = $this->getDoctrine()->getManager();
                $em->persist($template);
                $em->flush();

                $this->addFlash('success', $Trans->trans('layoutcontent.messages.salvataggio_ok'));
            }


            return $this->redirectToRoute('layoutcontent_edit', ['id' => $template->getId()]);

        }

        $data = [
            'widget' => $widget,
            'scssVariables' => $scssVariables,
            'scssConfig' => $scssVars,
            'scss' => $scss,
            'twig' => $twig,
            'colors' => $colors,
            'textsize' => $textsize,
            'fontweight' => $textweight,
            'template' => $template,
            'moduleAssets' => $moduleAssets,
        ];

        return $this->render('admin/layoutcontent/edit.html.twig', $data);

    }

}
