<?php
// 02/01/17, 15.08
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttachmentRepository")
 * @ORM\Table(name="news_attachments")
 */
class NewsAttachment extends Attachment
{

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\News", inversedBy="attachments")
     */
    private $news;

    /**
     * @return mixed
     */
    public function getParent()
    {

        return $this->news;
    }

    /**
     * @param mixed $news
     */
    public function setParent($news)
    {

        $this->token = '';
        $this->parent_id = $news->getId();
        $this->news = $news;
    }
}