<?php
// 19/01/17, 15.49
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SettingsRepository")
 * @ORM\Table(name="settings", uniqueConstraints={@UniqueConstraint(name="unicita_gruppo", columns={"chiave", "group_id"})})
 * @UniqueEntity(
 *     fields={"chiave", "group"},
 *     errorPath="chiave",
 *     message="Esiste già una chiave con questo nome per questo gruppo"
 * )
 */
class Settings
{

    use Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SettingsGroup", inversedBy="settings")
     */
    private $group;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string")
     */
    private $chiave;

    /**
     * @ORM\Column(type="string")
     */
    private $etichetta;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $aiuto;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $valore;

    /**
     * @ORM\Column(type="string")
     */
    private $requiredRole;

    /**
     * @ORM\Column(type="string")
     */
    private $tipo;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {

        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {

        $this->group = $group;
    }

    /**
     * @return mixed
     */
    public function getChiave()
    {

        return $this->chiave;
    }

    /**
     * @param mixed $chiave
     */
    public function setChiave($chiave)
    {

        $this->chiave = $chiave;
    }

    /**
     * @return mixed
     */
    public function getEtichetta()
    {

        return $this->etichetta;
    }

    /**
     * @param mixed $etichetta
     */
    public function setEtichetta($etichetta)
    {

        $this->etichetta = $etichetta;
    }

    /**
     * @return mixed
     */
    public function getAiuto()
    {

        return $this->aiuto;
    }

    /**
     * @param mixed $aiuto
     */
    public function setAiuto($aiuto)
    {

        $this->aiuto = $aiuto;
    }

    /**
     * @return mixed
     */
    public function getValore()
    {

        return $this->valore;
    }

    /**
     * @param mixed $valore
     */
    public function setValore($valore)
    {

        $this->valore = $valore;
    }

    /**
     * @return mixed
     */
    public function getRequiredRole()
    {

        return $this->requiredRole;
    }

    /**
     * @param mixed $requiredRole
     */
    public function setRequiredRole($requiredRole)
    {

        $this->requiredRole = $requiredRole;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {

        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {

        $this->tipo = $tipo;
    }

}