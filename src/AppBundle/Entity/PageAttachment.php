<?php
// 02/01/17, 15.08
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttachmentRepository")
 * @ORM\Table(name="pages_attachments")
 */
class PageAttachment extends Attachment
{

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Page", inversedBy="attachments")
     */
    private $page;

    /**
     * @return mixed
     */
    public function getParent()
    {

        return $this->page;
    }

    /**
     * @param mixed $product
     */
    public function setParent($page)
    {

        $this->token = '';
        $this->parent_id = $page->getId();
        $this->page = $page;
    }

}