<?php
// 30/12/16, 8.19
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttachmentRepository")
 * @ORM\Table(name="attachments")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="parent", type="string")
 * @ORM\DiscriminatorMap({
 *     "AppBundle\Entity\Page" = "AppBundle\Entity\PageAttachment",
 *     "AppBundle\Entity\News" = "AppBundle\Entity\NewsAttachment",
 *     "OfferteBundle\Entity\Offerta" = "OfferteBundle\Entity\OffertaAttachment",
 *     "AppartamentiBundle\Entity\Appartamento" = "AppartamentiBundle\Entity\AppartamentoAttachment",
 *     "Webtek\EcommerceBundle\Entity\Product" = "Webtek\EcommerceBundle\Entity\ProductAttachment",
 *     "AziendaBundle\Entity\Dipendente" = "AziendaBundle\Entity\DipendenteAttachment"
 *     })
 * @Vich\Uploadable()
 */
abstract class Attachment
{

    use ORMBehaviours\Sortable\Sortable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $size;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @Vich\UploadableField(mapping="allegati", fileNameProperty="name")
     *
     * @var File;
     */
    private $file;

    /**
     * @ORM\Column(type="integer")
     */
    protected $parent_id;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $token;

    function __toString()
    {

        return (string)$this->translate()->getAlt();
    }


    /**
     * @return mixed
     */
    public function getToken()
    {

        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {

        $this->token = $token;
        $this->parent_id = 0;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {

        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {

        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {

        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {

        return $this->file;
    }

    /**
     * @param File|UploadedFile $file
     *
     * @return $attachment
     */
    public function setFile(File $file = null)
    {

        $this->file = $file;
        if ($file) {
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {

        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {

        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {

        return $this->parent_id;
    }

    /**
     * @param mixed $parent_id
     */
    public function setParentId($parent_id)
    {

        $this->parent_id = $parent_id;
    }


}