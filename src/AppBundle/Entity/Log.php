<?php
// 28/09/17, 9.03
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * @ORM\Entity
 * @ORM\Table(name="log")
 */
class Log
{

    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $entityId;

    /**
     * @ORM\Column(type="string")
     */
    private $entityClass;

    /**
     * @ORM\Column(type="text")
     */
    private $log;

    /**
     * @ORM\Column(type="string")
     */
    private $author;

    /**
     * @ORM\Column(type="string")
     */
    private $action;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLog()
    {

        return $this->log;
    }

    /**
     * @param mixed $log
     */
    public function setLog($log)
    {

        $this->log = $log;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {

        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {

        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getEntityId()
    {

        return $this->entityId;
    }

    /**
     * @param mixed $entityId
     */
    public function setEntityId($entityId)
    {

        $this->entityId = $entityId;
    }

    /**
     * @return mixed
     */
    public function getEntityClass()
    {

        return $this->entityClass;
    }

    /**
     * @param mixed $entityClass
     */
    public function setEntityClass($entityClass)
    {

        $this->entityClass = $entityClass;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {

        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action)
    {

        $this->action = $action;
    }


}