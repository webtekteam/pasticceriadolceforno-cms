<?php
// 02/01/17, 15.08
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\Entity;

use AppBundle\Entity\Attachment;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttachmentRepository")
 * @ORM\Table(name="appartamenti_attachments")
 */
class AppartamentoAttachment extends Attachment
{

    /**
     * @ORM\ManyToOne(targetEntity="AppartamentiBundle\Entity\Appartamento", inversedBy="attachments")
     */
    private $appartamento;

    /**
     * @return mixed
     */
    public function getParent()
    {

        return $this->appartamento;
    }

    /**
     * @param mixed $appartamento
     */
    public function setParent($appartamento)
    {

        $this->token = '';
        $this->parent_id = $appartamento->getId();
        $this->appartamento = $appartamento;
    }

}