<?php

namespace GeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="GeoBundle\Repository\NazioniRepository")
 * @ORM\Table(name="countries")
 */
class Nazioni
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $countryCode;

    private $nome;

    /**
     * @return mixed
     */
    public function getCountryCode()
    {

        return $this->countryCode;
    }

    /**
     * @param mixed $countryCode
     */
    public function setCountryCode($countryCode)
    {

        $this->countryCode = $countryCode;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {

        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {

        return (string)$this->nome;
    }

}
