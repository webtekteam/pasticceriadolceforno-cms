<?php
// 14/03/17, 12.02
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Service;

use OfferteBundle\Entity\OffertaRow;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class OfferteDirectoryNamer implements DirectoryNamerInterface
{

    public function directoryName($object, PropertyMapping $mapping)
    {

        $dir = $mapping->getUriPrefix();

        $Offerta = $object;

        if ($Offerta instanceof OffertaRow) {

            $Offerta = $object->getOfferta();

        }

        $dir = $Offerta->getId();

        return $dir;

    }


}
