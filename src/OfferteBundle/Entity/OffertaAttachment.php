<?php
// 02/01/17, 15.08
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Entity;

use AppBundle\Entity\Attachment;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttachmentRepository")
 * @ORM\Table(name="offerta_attachments")
 */
class OffertaAttachment extends Attachment
{

    /**
     * @ORM\ManyToOne(targetEntity="OfferteBundle\Entity\Offerta", inversedBy="attachments")
     */
    private $offerta;

    /**
     * @return mixed
     */
    public function getParent()
    {

        return $this->offerta;
    }

    /**
     * @param mixed $offerta
     */
    public function setParent($offerta)
    {

        $this->token = '';
        $this->parent_id = $offerta->getId();
        $this->offerta = $offerta;
    }

}
